<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user())
        {
            Session::flash('errmsg', 'ثبت نام انجام نشده');
            return redirect()->route('site.login');
        }
        $user = auth()->user();
        if( $user->type != '2')
        {
            Session::flash('errmsg', 'دسترسی غیر مجاز به پنل ادمین');
            return redirect()->route('site.login');
        }
        return $next($request);
    }
}
