<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SendCodeForm;
use App\Http\Requests\RegisterForm;

class RegisterController extends Controller
{
    public function show()
    {
        return view('site.register');
    }

    public function sendCode(SendCodeForm $form)
    {
       return $form->store();
    }

    public function register(RegisterForm $form,$mobile)
    {
        return $form->check($mobile);
    }
}
