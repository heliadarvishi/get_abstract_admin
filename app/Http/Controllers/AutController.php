<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginForm;
use App\Http\Requests\ForgetPassForm;
use App\Http\Requests\SmsForm;
use App\Http\Requests\RessetPasswordForm;

class AutController extends Controller
{
    public function show()
    {
        return view('site.login');
    }

    public function login(LoginForm $form)
    {
        return $form->check();
    }

    public function logout()
    {
        auth()->logout();
        return view('site.login');
    }

    public function sendCode(ForgetPassForm $form)
    {
        return $form->store();
    }

    public function sendSmsCode($mobile)
    {
        return view('site.send_sms_code',compact('mobile'));
    }

    public function codeVerification(SmsForm $form,$mobile)
    {
        return $form->store($mobile);
    }

    public function ressetPassword(RessetPasswordForm $form,$mobile,$verification_code)
    {
        return $form->store($mobile,$verification_code);
    }

    public function ressetPass($mobile,$verification_code)
    {
        return view('site.resset_pass',compact('mobile','verification_code','status'));
    }  

    public function successRessetPass()
    {
        return view('site.success_resset_pass');
    }
}
