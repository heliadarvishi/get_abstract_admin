<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\Category;
use App\Models\Follow;
use App\Models\Summary;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\SearchForm;

class AuthorChannelController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $categories = Category::all();
        session()->forget('msge');
        $state = 0;
        $channels = Channel::where('user_id','<>',null)->latest()->with('category','user')->paginate(10);
        return view('admin.authorChannel',compact('user','channels','categories','state'));
    }

    public function changeStatePending(Channel $channel)
    {
        $channel-> update(["status" => "2"]);
        return redirect()->back()->with('message',' وضعیت "'.$channel->title.'" به حالت در انتظار تایید تغییر یافت ');
    }

    public function changeStateAccept(Channel $channel)
    {
        $channel-> update(["status" => "1"]);
        return redirect()->back()->with('message',' وضعیت "'.$channel->title.'" به حالت پذیرفته شده تغییر یافت ');
    }
    public function changeStateReject(Channel $channel)
    {
        $channel-> update(["status" => "3"]);
        return redirect()->back()->with('message',' وضعیت "'.$channel->title.'" به حالت پذیرفته شده تغییر یافت ');
    }

    public function destroy(Channel $channel)
    {
        Follow::deleteFollowByChannelId($channel->id);
        $summaries = Summary::where('channel_id',$channel->id)->get();
        foreach($summaries as $summary)
            Summary::destroy($summary);
        Channel::destroy($channel);
        Session::flash('message','کانال مورد نظر به همراه تمامی چکیده ها و تعلقاتش حذف شد');
        return redirect()->back();
    }

    public function search(SearchForm $form)
    {
        return $form->authorchannelSearch();
    }
}
