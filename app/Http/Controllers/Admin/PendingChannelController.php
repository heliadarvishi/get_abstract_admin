<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\Category;
use App\Http\Requests\SearchForm;

class PendingChannelController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $categories = Category::all();
        $channels = Channel::where('status','2')->latest()->with('category','user')->paginate(10);
        session()->forget('msge');
        $state = 0;
        return view('admin.pendingChannel',compact('user','channels','categories','state'));
    }

    public function search(SearchForm $form)
    {
        return $form->pendingChannelSearch();
    }
}
