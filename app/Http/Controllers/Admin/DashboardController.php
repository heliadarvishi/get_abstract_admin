<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Developer\PasswordForm;
use App\Http\Requests\Developer\InfoForm;
use App\Http\Requests\Admin\ProfileForm;
use App\Models\User;
use App\Models\Channel;
use App\Models\Summary;

class DashboardController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $authorCount      = User::where('type','3')->count();
        $superAuthorCount = User::where('type','4')->count();
        $channelCount     = Channel::all()->count();
        $summaryCount     = Summary::all()->count();
        return view('admin.dashboard',compact('user','authorCount','superAuthorCount','channelCount','summaryCount'));
    }

    public function editPass(PasswordForm $form)
    {
        return $form->edit();
    }

    public function editInfo(InfoForm $form)
    {
        return $form->edit();
    }

    public function profileStore(ProfileForm $form)
    {
        $admin = auth()->user();
        $pic = $form->store();
        $array = array_filter($form->all());
        if($pic)
        {
            $array['avatar'] = $pic;
            return $admin->editProfilePic($array);
        }
        return redirect()->back();   
    }
}
