<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Summary;
use App\Models\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\SearchForm;

class SummaryController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $summaries = Summary::latest()->with('channel','user','file')->paginate(10);
        session()->forget('msge');
        $state = 0;
        return view('admin.summary',compact('user','summaries','state'));
    }

    public function changeStatePending(Summary $summary)
    {
        $summary-> update(["status" => "2"]);
        return redirect()->back()->with('message',' وضعیت "'.$summary->title.'" به حالت در انتظار تایید تغییر یافت ');
    }

    public function changeStateAccept(Summary $summary)
    {
        $summary-> update(["status" => "1"]);
        return redirect()->back()->with('message',' وضعیت "'.$summary->title.'" به حالت پذیرفته شده تغییر یافت ');
    }
    public function changeStateReject(Summary $summary)
    {
        $summary-> update(["status" => "3"]);
        return redirect()->back()->with('message',' وضعیت "'.$summary->title.'" به حالت پذیرفته شده تغییر یافت ');
    }

    public function destroy(Summary $summary)
    {
        File::deleteFile($summary);
        $destinationPath = '/images/summary_cover/';
        Storage::disk('ftp')->delete($destinationPath.'/'.$summary['cover']);
        $summary->delete();
        return redirect()->back()->with('message',' چکیده ی '.$summary->title.'حذف شد');
    }

    public function search(SearchForm $form)
    {
        return $form->summarySearch();
    }
}
