<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AuthorForm;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Summary;
use App\Models\Channel;
use App\Models\Follow;
use App\Models\File;
use App\Http\Requests\SearchForm;

class AuthorController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        session()->forget('msge');
        $state = 0;
        $authors = User::where('type','3')->paginate(10);
        return view('admin.author',compact('user','authors','state'));
    }

    public function store(AuthorForm $form)
    {
        return $form->store();
    }

    public function destroy(User $author)
    {
        File::deleteFileByUserId($author->id);
        Summary::deleteSummaryByUserId($author->id);
        Follow::deleteFollowByUserId($author->id);
        Channel::deleteChannelByUserId($author->id);
        User::deleteUser($author);
        Session::flash('message','نویسنده مورد نظر به همراه تمامی آثار و تعلقاتش حذف شد');
        return redirect()->back();
    }

    public function search(SearchForm $form)
    {
        return $form->authorStore();
    }

    public function acceptRequest($id)
    {
       User::changeRequestToAccept($id,4);
       Channel::changeRequestToAccept($id);
       Summary::changeRequestToAccept($id);
       Session::flash('message','درخواست ارتقای نویسنده پذیرفته شد');
       return redirect()->back();
    }

    public function rejectrequest($id)
    {
        $user = User::find($id);
        $user->update(["request"=>3]);
        Session::flash('message','درخواست ارتقای نویسنده لغو شد');
        return redirect()->back();
    }
}
