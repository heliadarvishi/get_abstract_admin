<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Category;
use App\Models\Channel;
use App\Models\Follow;
use App\Models\File;
use App\Models\Summary;
use App\Http\Requests\Admin\ChannelForm;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\SearchForm;

class ChannelController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $categories = Category::all();
        $channels = Channel::where('user_id',null)->latest()->with('category')->paginate(10);
        session()->forget('msge');
        $state = 0;
        return view('admin.channel',compact('user','categories','channels','state'));
    }

    public function store(ChannelForm $form)
    {
        return $form->store();
    }

    public function destroy(Channel $channel)
    {
        Follow::deleteFollowByChannelId($channel->id);
        $summaries = Summary::where('channel_id',$channel->id)->get();
        foreach($summaries as $summary)
            Summary::destroy($summary);
        Channel::destroy($channel);
        Session::flash('message','کانال مورد نظر به همراه تمامی چکیده ها و تعلقاتش حذف شد');
        return redirect()->back();
    }

    public function search(SearchForm $form)
    {
        return $form->channelSearch();
    }
}
