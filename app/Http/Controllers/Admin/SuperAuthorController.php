<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SuperAuthorForm;
use App\Models\User;
use App\Models\Summary;
use App\Models\Channel;
use App\Models\Follow;
use App\Models\File;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\SearchForm;

class SuperAuthorController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $super_authors = User::where('type','4')->paginate(10);
        session()->forget('msge');
        $state = 0;
        return view('admin.superAuthor',compact('user','super_authors','state'));
    }

    public function store(SuperAuthorForm $form)
    {
        return $form->store();
    }

    public function destroy(User $author)
    {
        File::deleteFileByUserId($author->id);
        Summary::deleteSummaryByUserId($author->id);
        Follow::deleteFollowByUserId($author->id);
        Channel::deleteChannelByUserId($author->id);
        User::deleteUser($author);
        Session::flash('message','نویسنده مورد نظر به همراه تمامی آثار و تعلقاتش حذف شد');
        return redirect()->back();
    }

    public function search(SearchForm $form)
    {
        return $form->superAuthorStore();
    }

    public function acceptRequest($id)
    {
       User::changeRequestToAccept($id,3);
       Session::flash('message','درخواست تبدیل به نویسنده ی معمولی شدن پذیرفته شد');
       return redirect()->back();
    }

    public function rejectrequest($id)
    {
        $user = User::find($id);
        $user->update(["request"=>3]);
        Session::flash('message','درخواست تبدیل به نویسنده ی معمولی شدن لغو شد');
        return redirect()->back();
    }
}
