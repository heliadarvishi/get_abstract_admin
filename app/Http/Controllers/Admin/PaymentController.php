<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        return view('admin.payment',compact('user'));
    }
}
