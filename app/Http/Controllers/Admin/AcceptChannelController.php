<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Channel;
use App\Http\Requests\SearchForm;

class AcceptChannelController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $categories = Category::all();
        $channels = Channel::where([
            ['status','1'],
            ['user_id','!=',null]
            ])->latest()->with('category','user')->paginate(10);
        session()->forget('msge');
        $state = 0;
        return view('admin.acceptChannel',compact('user','categories','channels','state'));
    }

    public function search(SearchForm $form)
    {
        return $form->acceptChannelSearch();
    }
}
