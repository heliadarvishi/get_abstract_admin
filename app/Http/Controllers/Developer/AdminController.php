<?php

namespace App\Http\Controllers\Developer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Developer\AdminForm;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class AdminController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $admins = User::where('type','2')->latest()->paginate(10);
        return view('developer.admin',compact('user','admins'));
    }

    public function store(AdminForm $form)
    {
        return $form->store();
    }

    public function destroy($id)
    {
        $user = User::where('id',$id)->delete();
        Session::flash('message','ادمین مورد نظر حذف شد');
        return redirect()->back();
    }
}
