<?php

namespace App\Http\Controllers\Developer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Developer\PasswordForm;
use App\Http\Requests\Developer\InfoForm;

class DashboardController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        return view('developer.dashboard',compact('user'));
    }

    public function editPass(PasswordForm $form)
    {
        return $form->edit();
    }

    public function editInfo(InfoForm $form)
    {
        return $form->edit();
    }
}
