<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Sms;

class SmsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "code" => "required"
        ];
    }

    public function messages()
    {
        return [
            'required'  => 'پر کردن فیلد :attribute اجباری می باشد',
        ];
    }

    public function attributes()
    {
        return [
            'code'  => 'نام کاربری',
        ];
    }

    public function store($mobile)
    {
        return Sms::store($this->only('code'),$mobile);
    }
}
