<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use App\Models\Sms;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class SendCodeForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobile' => 'required|max:11'
        ];
    }
    public function store()
    {
        $mobile = $this->mobile;
        $user = new User;
        if($user->checkExistmobile($mobile))
        {
            Session::flash('error','!این شماره موبایل از قبل در سیستم ثبت شده');
            return redirect()->back();
        }
       
        $code = random_int(1111,9999);
        // Smsirlaravel::ultraFastSend(['VerificationCode'=>$code],16528,$this->mobile);
        $sms = Sms::firstOrCreate(['mobile'=> $mobile]);
        $sms->code = $code;
        $sms->save();
        return redirect("http://author.pielem.ir/register/$sms->id");
    }
}
