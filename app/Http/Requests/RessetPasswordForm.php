<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class RessetPasswordForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "new_password"     => "required",
            "confirm_password" => "required"
        ];
    }

    public function messages()
    {
        return [
            'required'  => 'پر کردن فیلد :attribute اجباری می باشد',
        ];
    }

    public function attributes()
    {
        return [
            'new_password'     => 'پسورد جدید',
            'confirm_password' => 'تکرار پسورد جدید',
        ];
    }

    public function store($mobile,$verification_code)
    {
        return User::ressetPass($this->only('new_password','confirm_password'),$mobile,$verification_code);
    }
}
