<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class ForgetPassForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "mobile" => "required"
        ];
    }

    public function messages()
    {
        return [
            'required'  => 'پر کردن فیلد :attribute اجباری می باشد',
        ];
    }

    public function attributes()
    {
        return [
            'user_name'  => 'شماره موبایل',
        ];
    }

    public function store()
    {
        return User::sendCode($this->only('mobile'));
    }
}
