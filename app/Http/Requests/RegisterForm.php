<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class RegisterForm extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code'   => 'required|max:4'
        ];
    }

    public function check($mobile)
    {
        return User::register($this->code,$mobile);
    }
}
