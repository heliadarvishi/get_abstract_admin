<?php

namespace App\Http\Requests\Developer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class InfoForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_name"  => "required",
            "first_name" => "required",
            "last_name"  => "required"
        ];
    }

    public function messages()
    {
        return [
            'required' => 'پر کردن فیلد :attribute اجباری می باشد',
            'mimes'    => 'فرمت  :attribute استاندارد عکس نیست',
            'numeric'  => 'فیلد :attribute فقط می تواند عدد باشد',
            'string'   => 'فیلد :attribute فقط می تواند متن باشد',
        ];
    }

    public function attributes()
    {
        return [
            'user_name'  => 'کلمه عبور',
            'first_name' => 'نام',
            'last_name'  => 'نام خانوادگی',
        ];
    }

    public function edit()
    {
        return User::editInfo($this->all());
    }
}
