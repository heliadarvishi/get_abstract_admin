<?php

namespace App\Http\Requests\Developer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class AdminForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => 'required',
            "last_name"  => 'required',
            "mobile"     => 'required',
            "password"   => 'required'
        ];
    }

    public function store()
    {
        return User::addAdmin($this->all());
    }
}
