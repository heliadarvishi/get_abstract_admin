<?php

namespace App\Http\Requests\Developer;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class PasswordForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "old_password"          => "required",
            "password"              => "required",
            "password_confirmation" => "required"
        ];
    }

    public function messages()
    {
        return [
            'required' => 'پر کردن فیلد :attribute اجباری می باشد',
            'mimes' => 'فرمت  :attribute استاندارد عکس نیست',
            //'numeric' => 'فیلد :attribute فقط می تواند عدد باشد',
            'string' => 'فیلد :attribute فقط می تواند متن باشد',
        ];
    }

    public function attributes()
    {
        return [
            'old_password'           => 'پسورد فعلی',
            'password'               => 'پسورد جدید',
            'password_confirmation'  => 'تکرار پسورد جدید',
        ];
    }

    public function edit()
    {
        return User::edit_password($this->all());
    }
}
