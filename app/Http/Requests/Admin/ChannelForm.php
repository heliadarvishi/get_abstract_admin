<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Channel;

class ChannelForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "type"  => "required",
            "title" => "required",
            "logo"  => "required|mimes:jpeg,png,jpg"
        ];
    }

    public function messages()
    {
        return [
            'required' => 'پر کردن فیلد :attribute اجباری می باشد',
            'mimes'    => 'فرمت  :attribute استاندارد عکس نیست',
            'numeric'  => 'فیلد :attribute فقط می تواند عدد باشد',
            'string'   => 'فیلد :attribute فقط می تواند متن باشد',
        ];
    }

    public function attributes()
    {
        return [
            'first_name' => 'نام',
            'last_name'  => 'نام خانوادگی',
            'mobile'     => 'موبایل',
            'password'   => 'پسورد',
            'txt'        => 'متن',
            'avatar'     => 'تصویر پروفایل'
        ];
    }

    public function store()
    {
        $file = $this->file('logo');
        return Channel::store($this->only('title','type'),$file);
    }
}
