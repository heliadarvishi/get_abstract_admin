<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class AuthorForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => 'required',
            "last_name"  => 'required',
            "mobile"     => 'required|numeric',
            "password"   => 'required',
            "avatar"     => 'mimes:jpeg,png,jpg',
            "about"      => 'string'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'پر کردن فیلد :attribute اجباری می باشد',
            'mimes'    => 'فرمت  :attribute استاندارد عکس نیست',
            'numeric'  => 'فیلد :attribute فقط می تواند عدد باشد',
            'string'   => 'فیلد :attribute فقط می تواند متن باشد',
        ];
    }

    public function attributes()
    {
        return [
            'first_name' => 'نام',
            'last_name'  => 'نام خانوادگی',
            'mobile'     => 'موبایل',
            'password'   => 'پسورد',
            'txt'        => 'متن',
            'avatar'     => 'تصویر پروفایل',
            'about'     => 'درباره نویسنده'
        ];
    }

    public function store()
    {
        $file = $this->file('avatar');
        return User::addAuthor($this->all(),$file);
    }
}
