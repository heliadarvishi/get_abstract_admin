<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ProfileForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "avatar" => "mimes:jpeg,png,jpg"
        ];
    }

    public function messages()
    {
        return [
            'required' => 'پر کردن فیلد :attribute اجباری می باشد',
            'mimes' => 'فرمت  :attribute استاندارد عکس نیست',
            //'numeric' => 'فیلد :attribute فقط می تواند عدد باشد',
            'string' => 'فیلد :attribute فقط می تواند متن باشد',
        ];
    }

    public function attributes()
    {
        return [
            'avatar'  => 'تصویر پروفایل'
        ];
    }

    public function store()
    {
        if ($this->file('avatar') != null)
        {
            $user = auth()->user();
            $file =  $this->file('avatar');
            $orginfilename = $file->getClientOriginalName();
            $destinationPath = '/images/profile_pic/';
            $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;
            Storage::delete($file);
            // Storage::disk('ftp')->put($destinationPath.$fileName,fopen($file, 'r+'));
            $resize = Image::make($file)->resize(100,100)->encode('jpg');
            Storage::disk('ftp')->put($destinationPath.$fileName,$resize);
            return $fileName;
        }
        return false;
    }
}
