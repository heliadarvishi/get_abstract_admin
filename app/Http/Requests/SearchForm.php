<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use App\Models\Channel;
use App\Models\Summary;

class SearchForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "search" => ""
        ];
    }

    public function authorStore()
    {
        return User::authorSearch($this->only('search'));
    }

    public function superAuthorStore()
    {
        return User::superAuthorSearch($this->only('search'));
    }

    public function authorchannelSearch()
    {
        return Channel::authorchannelSearch($this->only('search'));
    }

    public function channelSearch()
    {
        return Channel::ChannelSearch($this->only('search'));
    }

    public function acceptChannelSearch()
    {
        return Channel::acceptChannelSearch($this->only('search'));
    } 

    public function pendingChannelSearch()
    {
        return Channel::pendingChannelSearch($this->only('search'));
    } 

    public function rejectChannelController()
    {
        return Channel::rejectChannelController($this->only('search'));
    } 

    public function summarySearch()
    {
        return Summary::summarySearch($this->only('search'));
    } 
}
