<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $fillable = ['user_id','summary_id','type','url'];
    public function summary()
    {
        return $this->belongsTo(Summary::class);
    }

    public static function deleteFile($summary)
    {
        $files = $summary->file()->get();
        foreach($files as $file)
        {
            $destinationPath = '/images/summary_file/';
            Storage::disk('ftp')->delete($destinationPath.'/'.$file['url']);
            $file->delete();
        }
    }

    public static function deleteFileByUserId($id)
    {
        $files = File::where('user_id',$id)->get();
        $destinationPath = '/images/summary_file/';
        foreach($files as $file)
        {
            Storage::disk('ftp')->delete($destinationPath.'/'.$file['url']);
            $file->delete();
        } 
    }

    public static function deleteFileBySummaryId($id)
    {
        $files = File::where('summary_id',$id)->get();
        $destinationPath = '/images/summary_file/';
        foreach($files as $file)
        {
            Storage::disk('ftp')->delete($destinationPath.'/'.$file['url']);
            $file->delete();
        } 
    }
}
