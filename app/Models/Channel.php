<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
class Channel extends Model
{
    protected $fillable = ['title','logo','category_id','user_id','status'];

    public function summary()
    {
        return $this->hasMany(Summary::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function store($data,$file)
    {
        $user = auth()->user();
        // if ($file != null)
        //     $orginfilename = Input::file('logo')->getClientOriginalName();
        // $destinationPath = 'upload/images/channel_logo';
        // $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;
        // $file->move($destinationPath, $fileName);

        if ($file != null)
        $orginfilename = Input::file('logo')->getClientOriginalName();
        $destinationPath = '/images/channel_logo/';
        $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;
        Storage::disk('ftp')->put($destinationPath.$fileName,fopen($file, 'r+'));
        
        $channel = Channel::firstOrCreate([
            'title'       => $data['title'],
            'logo'        => $fileName,
            'category_id' => $data['type'],
            'status'      => '1'
        ]);

        Session::flash('message','کانال مورد نظر ثبت شد');
        return redirect()->back();
    }

    public static function deleteChannelByUserId($id)
    {
        $destinationPath = '/images/channel_logo/';
        $channels = Channel::where('user_id',$id)->get();
        foreach($channels as $channel)
        {
            Storage::disk('ftp')->delete($destinationPath.'/'.$channel['logo']); 
            $channel->delete();       
        }
    }

    public static function destroy($channel)
    {
        $destinationPath = '/images/channel_logo/';
        Storage::disk('ftp')->delete($destinationPath.'/'.$channel['logo']); 
        $channel->delete();       
    }

    public static function channelSearch($data)
    {
        $search = $data['search'];
        $channels = Channel::where('user_id',null)
            ->where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($channels->count()>0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 0;
            session()->forget('msge');
            return view('admin.channel',compact('user','channels','state','categories'));
        }
        elseif($channels->count() == 0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.channel',compact('user','channels','state','categories'));
        }
    }

    public static function authorchannelSearch($data)
    {
        $search = $data['search'];
        $channels = Channel::where('user_id','<>',null)
            ->where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')->latest()->with('category','user')
            ->paginate(10);

        if($channels->count()>0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 0;
            session()->forget('msge');
            return view('admin.authorChannel',compact('user','channels','state','categories'));
        }
        elseif($channels->count() == 0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.authorChannel',compact('user','channels','state','categories'));
        }
    }

    public static function acceptChannelSearch($data)
    {
        $search = $data['search'];
        $channels = Channel::where('status','1')
            ->where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($channels->count()>0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 0;
            session()->forget('msge');
            return view('admin.acceptChannel',compact('user','channels','state','categories'));
        }
        elseif($channels->count() == 0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.acceptChannel',compact('user','channels','state','categories'));
        }
    }
    
    public static function pendingChannelSearch($data)
    {
        $search = $data['search'];
        $channels = Channel::where('status','2')
            ->where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($channels->count()>0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 0;
            session()->forget('msge');
            return view('admin.pendingChannel',compact('user','channels','state','categories'));
        }
        elseif($channels->count() == 0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.pendingChannel',compact('user','channels','state','categories'));
        }
    }

    public static function rejectChannelController($data)
    {
        $search = $data['search'];
        $channels = Channel::where('status','3')
            ->where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($channels->count()>0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 0;
            session()->forget('msge');
            return view('admin.rejectChannel',compact('user','channels','state','categories'));
        }
        elseif($channels->count() == 0)
        {
            $user = auth()->user();
            $categories = Category::all();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.rejectChannel',compact('user','channels','state','categories'));
        }
    }  
    
    public static function changeRequestToAccept($id)
    {
        $channels = Channel::where('user_id',$id)->get();
        foreach($channels as $channel)
        {
            $channel->update(["status" => "1"]);
        }
    }
}
