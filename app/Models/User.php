<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Ipecompany\Smsirlaravel\Smsirlaravel;
use Laravel\Passport\HasApiTokens;
use File;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password','mobile','user_name','avatar','first_name','type','last_name','about','request'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sammary()
    {
        return $this->hasMany(Summary::class);
    }

    public function channel()
    {
        return $this->hasMany(Channel::class);
    }

    public static function login($data)
    {
        $developer = User::where([
            ['mobile', $data['mobile']],
            ['type', '5']
        ])->first();

        $admin = User::where([
            ['mobile', $data['mobile']],
            ['type', '2']
        ])->first();
        
        if ($developer == null && $admin == null)
        {
            Session::flash('errmsg', 'نام کاربری یا رمز عبور اشتباه است');
            return redirect()->route('site.login');
        }

        if ($developer != null)
        {
            $hashedPassword = $developer->getAuthPassword();
            if (Hash::check($data['password'], $hashedPassword))
            {
                Auth::login($developer);
                return redirect()->route('developer.dashboard');
            }
            else
            {
                Session::flash('errmsg', 'نام کاربری یا رمز عبور اشتباه است');
                return redirect()->route('site.login');
            }
              
        }

        if ($admin != null)
        {
            $hashedPassword = $admin->getAuthPassword();
            if (Hash::check($data['password'], $hashedPassword))
            {
                Auth::login($admin);
                return redirect()->route('admin.dashboard');
            }
            else
            {
                Session::flash('errmsg', 'نام کاربری یا رمز عبور اشتباه است');
                return redirect()->route('site.login');
            }
        }
    }

    public static function addAdmin($data)
    {
        $user = new User;
        if($user->checkExistmobile($data['mobile']))
        {
            Session::flash('error','موبایل از قبل در سیستم ثبت شده');
            return redirect()->back();
        }
        $password = Hash::make($data['password']);
        $user = $user->firstOrcreate([
            "first_name" => $data['first_name'],
            "last_name"  => $data['first_name'],
            "mobile"     => $data['mobile'],
            "password"   => $password,
            "type"       => '2'
        ]);
        $token = $user->createToken('user_token')->accessToken;
        Session::flash('message','ادمین مورد نظر ثبت شد');
        return redirect()->back();
    }

    public static function addAuthor($data,$file)
    {
        $user = auth()->user();
        $user = new User;
        if($user->checkExistmobile($data['mobile']))
        {
            Session::flash('error','موبایل از قبل در سیستم ثبت شده');
            return redirect()->back();
        }

        if ($file != null)
        {
            $orginfilename = Input::file('avatar')->getClientOriginalName();
            $destinationPath = '/images/profile_pic/';
            $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;
            Storage::disk('ftp')->put($destinationPath.$fileName,fopen($file, 'r+'));
        }
        else
        $fileName = 'account.png';
      
        $password = Hash::make($data['password']);
        $user = $user->firstOrcreate([
            "first_name" => $data['first_name'],
            "last_name"  => $data['last_name'],
            "mobile"     => $data['mobile'],
            "password"   => $password,
            "avatar"     => $fileName,
            "type"       => '3',
            "about"      => $data['about']
        ]);

        $token = $user->createToken('user_token')->accessToken;
       
        Session::flash('message','نویسنده مورد نظر ثبت شد');
        return redirect()->back();
    }

    public static function addSuperAuthor($data,$file)
    {
        $user = auth()->user();
        $user = new User;
        if($user->checkExistmobile($data['mobile']))
        {
            Session::flash('error','موبایل از قبل در سیستم ثبت شده');
            return redirect()->back();
        }

        if ($file != null)
        {
            $orginfilename = Input::file('avatar')->getClientOriginalName();
            $destinationPath = '/images/profile_pic/';
            $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;
            Storage::disk('ftp')->put($destinationPath.$fileName,fopen($file, 'r+'));
        }
        else
        $fileName = 'account.png';
       
        $password = Hash::make($data['password']);
        $user = $user->firstOrcreate([
            "first_name" => $data['first_name'],
            "last_name"  => $data['last_name'],
            "mobile"     => $data['mobile'],
            "password"   => $password,
            "avatar"     => $fileName,
            "type"       => '4',
            "about"      => $data['about']
        ]);

        $token = $user->createToken('user_token')->accessToken;
       
        Session::flash('message','نویسنده ویژه مورد نظر ثبت شد');
        return redirect()->back();
    }

    public function checkExistmobile($mobile)
    {
        $user = User::where('mobile',$mobile)->get();  
        if(count($user) != 0)
        return true;
        return false;
    }

    public static function edit_password($data)
    {
        $user = auth()->user();
        if(password_verify($data['old_password'],$user->password))
        {
            if ($data['password'] != $data['password_confirmation'])
            {
                Session::flash('error', 'پسورد جدید با تکرار پسورد مطابقت ندارد');
                return redirect()->back();
            }
            $password = Hash::make($data['password']);
            $user = $user -> update([
                "password" => $password
            ]);
            Session::flash('message',' پسورد به روزرسانی شد ');
            return redirect()->back();
        }
        else
        {
            Session::flash('error', 'پسورد فعلی اشتباه است');
            return redirect()->back();
        }
    }

    public static function editInfo($data)
    {
        $user = auth()->user();
        $user = $user -> update([
            "user_name"  => $data['user_name'],
            "first_name" => $data['first_name'],
            "last_name"  => $data['last_name'],
        ]);
        Session::flash('message',' اطلاعات به روزرسانی شد ');
        return redirect()->back();
    }

    public function editProfilePic($data)
    {
        self::update($data);
        return redirect()->back()->with('message','تصویر پروفایل شما  ثبت شد');
    }

    public static function deleteUser($user)
    {
        $destinationPath = '/images/profile_pic/';
        Storage::disk('ftp')->delete($destinationPath.'/'.$user['avatar']);
        $user->delete();
    }

    public static function sendCode($data)
    {
        $admin = User::where([
            ['mobile', $data['mobile']],
            ['type', '2']
        ])->first();

        if ($admin == null)
            return redirect()->back()->with('errmsg', 'اکانتی با این شماره موبایل، به عنوان ادمین ثبت نشده');
        else
        {
            $code = random_int(1111,9999);
            Smsirlaravel::ultraFastSend(['VerificationCode' => $code],16528,$data['mobile']);
            if(Sms::where('mobile',$data['mobile'])->get()->count() == 0) 
            {
                $sms = Sms::firstOrCreate(['mobile' => $data['mobile']]);
            }
            else
            {
                $sms = Sms::where('mobile',$data['mobile'])->first();
                $sms->update(['code'=> $code]);
            }     
            $mobile = $data['mobile'];     
            return view('site.send_sms_code',compact('mobile'));
        }
    }

    public static function ressetPass($info,$mobile,$verification_code)
    {
        $data = [
            'mobile'            => $mobile,
            'verification_code' => $verification_code
        ];
        if(Sms::where('mobile',$mobile)->first()->code != $verification_code)
        {
            $status = 2;
            return view('site.resset_pass',compact('data','status'))->with('errmsg','کد تایید نامعتبر است');
        }
        

        $user = User::where('mobile',$mobile)->first();
        if ($info['new_password'] != $info['confirm_password'])
        {
            session()->forget('message');
            Session::flash('errmsg','پسورد جدید با تکرار پسورد مطابقت ندارد');
         
            return view('site.resset_pass',compact('data'));
        }
        else
        {
            $password = Hash::make($info['new_password']);
            $user = $user -> update(["password" => $password]);

            //resset code_verification
            $code = random_int(1111,9999);
            $sms = Sms::where('mobile',$mobile)->first();
            $sms->update(['code'=> $code]);

            session()->forget('errmsg');
            Session::flash('message','پسورد شما به روز رسانی شد');
        
            return view('site.success_resset_pass');
        } 
    }

    public static function authorSearch($data)
    {
        $search = $data['search'];
        $authors = User::where('type','3')
            ->where(function($w) use($search) {
            $w->orWhere('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orWhere('mobile','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($authors->count()>0)
        {
            $user = auth()->user();
            $state = 0;
            session()->forget('msge');
            return view('admin.author',compact('user','authors','result','state'));
        }
        elseif($authors->count() == 0)
        {
            $user = auth()->user();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.author',compact('user','authors','result','state'));
        }
    }

    public static function superAuthorSearch($data)
    {
        $search = $data['search'];
        $super_authors = User::where('type','4')
            ->where(function($w) use($search) {
            $w->orWhere('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orWhere('mobile','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->paginate(10);

        if($super_authors->count()>0)
        {
            $user = auth()->user();
            $state = 0;
            session()->forget('msge');
            return view('admin.superAuthor',compact('user','super_authors','result','state'));
        }
        elseif($super_authors->count() == 0)
        {
            $user = auth()->user();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.superAuthor',compact('user','super_authors','result','state'));
        }
    }

    public static function register($code,$mobile)
    {
        $sms = Sms::where([
            ['mobile',$mobile],
            ['code',$code]
        ])->first();
        if(is_null($sms))
        {
            Session::flash('error','کد تایید صحیح نیست');
            return redirect()->back();
        }

        $user = User::where('mobile',$mobile)->first();
        if(is_null($user))
        {
            $user = new User;
            $user = $user->updateOrCreate([
                'mobile' => $data['mobile'],
                'type'   => '3'
            ]);
        }
        else
        { 
            Session::flash('error','!این شماره موبایل از قبل در سیستم ثبت شده');
            return redirect()->back();
        }
    }

    public static function changeRequestToAccept($id,$type)
    {
        $user = User::find($id);
        $user->update([
            "request" => null,
            "type"    => $type
        ]);
    }
}