<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public static function deleteFollowByUserId($id)
    {
        Follow::where('user_id',$id)->delete();
    }

    public static function deleteFollowByChannelId($id)
    {
        Follow::where([
            ['parent_id',$id],
            ['type','2']
        ])->delete();
    }
}
