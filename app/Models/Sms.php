<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = ['mobile', 'code'];
    
    public static function store($data,$mobile)
    {
        if(Sms::where('mobile',$mobile)->first()->code == $data['code'])
        {
            $data = [
                'mobile'            => $mobile,
                'verification_code' => $data['code']
            ];
            session()->forget('errmsg');
        
            return view('site.resset_pass',compact('data'));
        }
        else
        {
            Session::flash('errmsg',' کد تایید نامعتبر است');
            return view('site.send_sms_code',compact('mobile'));
        }
    }
}
