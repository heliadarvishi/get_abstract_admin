<?php

namespace App\Models;

use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class Summary extends Model
{
    protected $fillable =['user_id','title','channel_id','category_id','text','status'];
    public function file()
    {
        return $this->hasMany(File::class);
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function store($data)
    {
        $user = auth()->user();
        $summary = Summary::firstOrCreate([
            'title'       => $data['title'],
            'category_id' => $data['category_id'],
            'channel_id'  => $data['channel_id'],
            'user_id'     => $user->id
        ]);


    }

    // public function deleteSummary($summary)
    // {
    //     $user = auth()->user();
    //     $file = $summary->file()->first();
    //     if(isset($file))
    //         $file->delete();
    //     if($summary->user_id == $user->id)
    //         $summary->delete();
    //     return response()->json(true,200);
    // }

    public function updateSummary(Summary $summary,$data)
    {
        return Response()->json('true', 200);
    }

    public static function deleteSummaryByUserId($id)
    {
        $destinationPath = '/images/summary_cover/';
        $summaries = Summary::where('user_id',$id)->get();
        foreach($summaries as $summary)
        {
            Storage::disk('ftp')->delete($destinationPath.'/'.$summary['cover']);
            $summary->delete();
        }
    }

    public static function destroy($summary)
    {
        File::deleteFileBySummaryId($summary->id);
        $destinationPath = '/images/summary_cover/';
        Storage::disk('ftp')->delete($destinationPath.'/'.$summary['cover']);
        $summary->delete();
    }

    public static function summarySearch($data)
    {
        $search = $data['search'];
        $summaries = Summary::where(function($w) use($search) {
            $w->orWhere('title','like','%'.$search.'%');
            })->orderBy('id','Desc')
            ->with('channel','user','file')
            ->paginate(10);

        if($summaries->count()>0)
        {
            $user = auth()->user();
            $state = 0;
            session()->forget('msge');
            return view('admin.summary',compact('user','summaries','state'));
        }
        elseif($summaries->count() == 0)
        {
            $user = auth()->user();
            $state = 1;
            Session::flash('msge','موردی برای واژه ی "'. $data['search'].'" یافت نشد');
            return view('admin.summary',compact('user','summaries','state'));
        }
    }

    public static function changeRequestToAccept($id)
    {
        $summaries = Summary::where('user_id',$id)->get();
        foreach($summaries as $summary)
        {
            $summary->update(["status" => "1"]);
        }
    }
}
