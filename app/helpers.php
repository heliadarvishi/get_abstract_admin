<?php
use App\Models\Summary;
use App\Models\User;

function countSummaryByUserId($user_id)
{
    return Summary::where('user_id',$user_id)->get()->count();
}

function countSummaryByChannelId($channel_id)
{
    return Summary::where('channel_id',$channel_id)->get()->count();
}

function listSummaryByChannelId($channel_id)
{
    return Summary::where('channel_id',$channel_id)->get();
}

function getNameChannelMaker($user_id)
{
    return User::where('id',$user_id)->select('first_name','last_name')->first();
}
?>