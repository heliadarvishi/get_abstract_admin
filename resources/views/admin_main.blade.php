<!DOCTYPE html>
<html lang="en" dir="rtl">
    <head>
          @include('partials.admin._head')
    </head>
    <body class="fixed-left ">
        <div id="wrapper">
             @include('partials.admin._sidebar')
            <div class="content-page ">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        @yield('content')
                    </div> <!-- container -->
                </div> <!-- content -->
                   @include('partials.admin._footer')
            </div>
        </div>
        <!-- END wrapper -->
        @include('partials.admin._scripts')
    </body>
</html>