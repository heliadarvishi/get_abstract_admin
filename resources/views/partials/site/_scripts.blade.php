<script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NKDMSK6');
</script> 
<!--   Core JS Files   -->
<script src="{{URL::asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/moment.min.js')}}"></script>
<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<script src="{{URL::asset('assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{URL::asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDGat1sgDZ-3y6fFe6HD7QUziVC6jlJNog"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js')}}"></script>
<!--	Plugin for Sharrre btn -->
<script src="{{URL::asset('assets/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
<script src="{{URL::asset('assets/js/material-kit.min.js?v=2.0.6')}}" type="text/javascript"></script>
<script>

// Facebook Pixel Code Don't Delete
! function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
    n.callMethod ?
        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
}(window,
    document, 'script', '//connect.facebook.net/en_US/fbevents.js');

try {
    fbq('init', '111649226022273');
    fbq('track', "PageView");

} catch (err) {
    console.log('Facebook Track Error:', err);
}
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
</noscript>
<script>
$(document).ready(function() {
    //init DateTimePickers
    materialKit.initFormExtendedDatetimepickers();

    // Sliders Init
    materialKit.initSliders();
});


function scrollToDownload() {
    if ($('.section-download').length != 0) {
    $("html, body").animate({
        scrollTop: $('.section-download').offset().top
    }, 1000);
    }
}

function scrollToRegister() {
    if ($('.section-signup').length != 0) {
    $("html, body").animate({
        scrollTop: $('.section-signup').offset().top
    }, 1000);
    }
}

function scrollToLogin() {
    if ($('.section-examples').length != 0) {
    $("html, body").animate({
        scrollTop: $('.section-examples').offset().top
    }, 1000);
    }
}

$(document).ready(function() {

    $('#facebook').sharrre({
    share: {
        facebook: true
    },
    enableHover: false,
    enableTracking: false,
    enableCounter: false,
    click: function(api, options) {
        api.simulateClick();
        api.openPopup('facebook');
    },
    template: '<i class="fab fa-facebook-f"></i> Facebook',
    url: 'https://demos.creative-tim.com/material-kit/index.html'
    });

    $('#googlePlus').sharrre({
    share: {
        googlePlus: true
    },
    enableCounter: false,
    enableHover: false,
    enableTracking: true,
    click: function(api, options) {
        api.simulateClick();
        api.openPopup('googlePlus');
    },
    template: '<i class="fab fa-google-plus"></i> Google',
    url: 'https://demos.creative-tim.com/material-kit/index.html'
    });

    $('#twitter').sharrre({
    share: {
        twitter: true
    },
    enableHover: false,
    enableTracking: false,
    enableCounter: false,
    buttons: {
        twitter: {
        via: 'CreativeTim'
        }
    },
    click: function(api, options) {
        api.simulateClick();
        api.openPopup('twitter');
    },
    template: '<i class="fab fa-twitter"></i> Twitter',
    url: 'https://demos.creative-tim.com/material-kit/index.html'
    });

});
</script>


<script src="{{URL::asset('assets/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.stellar.min.js')}}"></script>
<script src="{{URL::asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{URL::asset('assets/js/aos.js')}}"></script>
<script src="{{URL::asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{URL::asset('assets/js/jquery.timepicker.min.js')}}"></script>
<script src="{{URL::asset('assets/js/scrollax.min.js')}}"></script>
<script src="{{URL::asset('assets/js/main.js')}}"></script>