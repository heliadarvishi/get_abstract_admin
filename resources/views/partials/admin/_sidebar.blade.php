<div class="topbar">

    <div class="topbar-left">
        <a href="" class="logo"><span style="color: #444;"> مدیریت </span><i class="zmdi zmdi-view-web"></i></a>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">

            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left">
                        <i class="zmdi zmdi-menu"></i>
                    </button>
                </li>
                <li>
                    <h4 class="page-title" style="font-family: iransans;">@yield('title')</h4>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <div class="user-box">
            <div class="user-img">
            <a href="" class="text-custom" data-toggle="modal" data-target="#profile{{$user->id}}"> 
                <img src="https://cdn.pielem.ir/uploads/images/profile_pic/{{$user->avatar}}" alt="" title="" class="img-circle img-thumbnail img-responsive">
            </a>
            </div>
            <br>
            
            <h5 style="font-family: iransans;color:#fff;">{{$user->first_name}} {{$user->last_name}}</h5>
            <a href="{{route('site.login')}}" style="color: #444;font-family: iransans;">برو به سایت</a>
            <br>
            <br>
            <ul class="list-inline">
                <li>
                    <a href="{{route('site.logout')}}" class="text-custom">
                        <i class="zmdi zmdi-power zmdi-hc-2x" style="color:#ee1c25;"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{route('admin.dashboard')}}" class="waves-effect {{ Request::is('admin/dashboard') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-account-box"></i> <span> داشبورد </span> </a>
                </li>

                <li>
                    <a href="{{route('admin.author')}}" class="waves-effect {{ Request::is('admin/author') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-edit"></i> <span> نویسنده های عادی </span> </a>
                </li>

                <li>
                    <a href="{{route('admin.superAuthor')}}" class="waves-effect {{ Request::is('admin/super-author') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-assignment-o"></i> <span> نویسنده های ویژه</span> </a>
                </li>

                <li class="has_sub" style="color:white;font-family: iransans;">
                    <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-volume-mute"></i><span>کانال ها</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{route('admin.channel')}}" class="waves-effect" style="font-family: iransans; {{ Request::is('admin/channel') ? "color:#00cccc;" : "" }}"><i class="zmdi zmdi-volume-mute"></i> <span>کانال های عمومی</span> </a>
                        </li>

                        <li>
                            <a href="{{route('admin.authorChannel')}}" class="waves-effect" style="font-family: iransans; {{ Request::is('admin/author-channel') ? "color:#00cccc;" : "" }}"><i class="zmdi zmdi-volume-mute"></i> <span>کانال های نویسنده ها</span> </a>
                        </li>

                        <li>
                            <a href="{{route('admin.acceptChannel')}}" class="waves-effect" style="font-family: iransans; {{ Request::is('admin/accept-channel') ? "color:#00cccc;" : "" }}"><i class="zmdi zmdi-volume-mute"></i> <span>کانال های پذیرفته شده</span> </a>
                        </li>
                        
                        <li>
                            <a href="{{route('admin.pendingChannel')}}" class="waves-effect" style="font-family: iransans; {{ Request::is('admin/pending-channel') ? "color:#00cccc;" : "" }}"><i class="zmdi zmdi-volume-mute"></i> <span>کانال های درانتظار تایید</span> </a>
                        </li>

                        <li>
                            <a href="{{route('admin.rejectChannel')}}" class="waves-effect" style="font-family: iransans; {{ Request::is('admin/reject-channel') ? "color:#00cccc;" : "" }}"><i class="zmdi zmdi-volume-mute"></i> <span>کانال های رد شده</span> </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('admin.summary')}}" class="waves-effect {{ Request::is('admin/summary') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-collection-text"></i> <span>چکیده ها</span> </a>
                </li>

                <li>
                    <a href="{{route('admin.payment')}}" class="waves-effect {{ Request::is('admin/payment') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-collection-text"></i> <span> پرداخت ها</span> </a>
                </li>
            </ul>

            <div class="clearfix"></div>
        </div>
    </div>

</div>
<!-- start modal -->
<div id="profile{{$user->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="font-family: iransans;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            </div>
            <div class="modal-body">
                <form action="{{route('dashboard.profile')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="file" class="dropify"  name="avatar" accept=".jpg , .png" data-default-file= "" data-max-file-size="2M"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end modal -->
