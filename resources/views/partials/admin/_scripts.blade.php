<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{URL::asset('admin/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('admin/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{URL::asset('admin/js/detect.js')}}"></script>
<script src="{{URL::asset('admin/js/fastclick.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.slimscroll.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.blockUI.js')}}"></script>
<script src="{{URL::asset('admin/js/waves.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.nicescroll.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.scrollTo.min.js')}}"></script>

<script src="{{ URL::asset('admin/js/plugins/jquery-knob/jquery.knob.js') }}"></script>

<!--form wysiwig js-->
<script src="{{ URL::asset('admin/js/plugins/tinymce/tinymce.min.js') }}"></script>

    <!-- Dashboard init -->
    <!-- <script src="{{ URL::asset('manager/js/pages/jquery.dashboard.js') }}"></script> -->

<!-- Validation js (Parsleyjs) -->
<script type="text/javascript" src="{{ URL::asset('admin/js/plugins/parsleyjs/dist/parsley.min.js') }}"></script>

<!-- App js -->
<script src="{{URL::asset('admin/js/jquery.core.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.app.js')}}"></script>

<!-- Modal-Effect -->
<script src="{{ URL::asset('admin/js/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/plugins/custombox/dist/legacy.min.js') }}"></script>

<!--form wysiwig js-->
<script src="{{ URL::asset('admin/js/plugins/tinymce/tinymce.min.js') }}"></script>

<script src="{{ URL::asset('admin/js/jquery.slimscroll.js') }}"></script>

<!-- file uploads js -->
<script src="{{ URL::asset('admin/js/plugins/fileuploads/js/dropify.min.js') }}"></script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'برای انتخاب فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'replace': 'برای جایگزینی فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'remove': 'حذف',
            'error': 'خطا در بارگذاری عکس'
        },
        error: {
            'fileSize': 'حجم عکس حداکثر 1M میتواند باشد'
        }
    });
</script>

<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'برای انتخاب فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'replace': 'برای جایگزینی فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'remove': 'حذف',
            'error': 'خطا در بارگذاری عکس'
        },
        error: {
            'fileSize': 'حجم عکس حداکثر 1M میتواند باشد'
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        if($("#elm1").length > 0){
            tinymce.init({
                selector: "textarea#elm1",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "rtl insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
    });
</script>

