<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon" href="{{URL::asset('admin/images/favicon.ico')}}">

<!-- App title -->
<title>Developer Get Abstracto</title>

<!-- App CSS -->
<link href="{{URL::asset('admin/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/core.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/components.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/icons.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/pages.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/menu.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('admin/css/responsive.css')}}" rel="stylesheet" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="assets/js/modernizr.min.js"></script>
        