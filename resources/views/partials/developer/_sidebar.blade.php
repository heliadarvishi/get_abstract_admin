<div class="topbar">

    <div class="topbar-left">
        <a href="" class="logo"><span style="color: #444;"> مدیریت </span><i class="zmdi zmdi-view-web"></i></a>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">

            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left">
                        <i class="zmdi zmdi-menu"></i>
                    </button>
                </li>
                <li>
                    <h4 class="page-title" style="font-family: iransans;">@yield('title')</h4>
                </li>
            </ul>

        </div>
    </div>
</div>
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <div class="user-box">
            <div class="user-img">
                <img src="{{URL::asset('panel/images/logo.png')}}" alt="" title="" class="img-circle img-thumbnail img-responsive">

            </div>
            <br>
            
            <h5 style="font-family: iransans;color:#fff;">{{$user->first_name}} {{$user->last_name}}</h5>
            <a href="{{route('site.login')}}" style="color: #444;font-family: iransans;">برو به سایت</a>
            <br>
            <br>
            <ul class="list-inline">
                <li>
                    <a href="{{route('site.logout')}}" class="text-custom">
                        <i class="zmdi zmdi-power zmdi-hc-2x" style="color:#ee1c25;"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End User -->

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{route('developer.dashboard')}}" class="waves-effect {{ Request::is('developer/dashboard') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-account-box"></i> <span> داشبورد </span> </a>
                </li>

                <li>
                    <a href="{{route('developer.admin')}}" class="waves-effect {{ Request::is('developer/admin') ? "active" : "" }}" style="font-family: iransans;"><i class="zmdi zmdi-account-box"></i> <span> افزودن ادمین </span> </a>
                </li>
            </ul>

            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

    </div>

</div>
