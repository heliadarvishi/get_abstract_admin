<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{URL::asset('admin/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('admin/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{URL::asset('admin/js/detect.js')}}"></script>
<script src="{{URL::asset('admin/js/fastclick.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.slimscroll.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.blockUI.js')}}"></script>
<script src="{{URL::asset('admin/js/waves.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.nicescroll.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.scrollTo.min.js')}}"></script>

<script src="{{ URL::asset('admin/js/plugins/jquery-knob/jquery.knob.js') }}"></script>

<!--form wysiwig js-->
<script src="{{ URL::asset('admin/js/plugins/tinymce/tinymce.min.js') }}"></script>

    <!-- Dashboard init -->
    <!-- <script src="{{ URL::asset('manager/js/pages/jquery.dashboard.js') }}"></script> -->

<!-- Validation js (Parsleyjs) -->
<script type="text/javascript" src="{{ URL::asset('admin/js/plugins/parsleyjs/dist/parsley.min.js') }}"></script>

<!-- App js -->
<script src="{{URL::asset('admin/js/jquery.core.js')}}"></script>
<script src="{{URL::asset('admin/js/jquery.app.js')}}"></script>

<!-- Modal-Effect -->
<script src="{{ URL::asset('admin/js/plugins/custombox/dist/custombox.min.js') }}"></script>
<script src="{{ URL::asset('admin/js/plugins/custombox/dist/legacy.min.js') }}"></script>

<!--form wysiwig js-->
<script src="{{ URL::asset('admin/js/plugins/tinymce/tinymce.min.js') }}"></script>

<!-- file uploads js -->
<script src="{{ URL::asset('admin/js/plugins/fileuploads/js/dropify.min.js') }}"></script>
<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'برای انتخاب فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'replace': 'برای جایگزینی فایل کلیک کنید و یا فایل را روی این فیلد بکشید',
            'remove': 'حذف',
            'error': 'خطا در بارگذاری عکس'
        },
        error: {
            'fileSize': 'حجم عکس حداکثر 1M میتواند باشد'
        }
    });
</script>

