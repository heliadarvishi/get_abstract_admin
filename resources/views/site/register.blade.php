@extends('site_main')
@section('body')
 <body class="login-page sidebar-collapse" style="font-family: 'Yekan','Roboto-Regular';">
@stop
@section('content')
  <div class="page-header header-filter" style="background-image: url('../assets/img/bg20.jpg'); background-size: cover; background-position: top center;">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 ml-auto mr-auto">
          <div class="card card-login">
            <form class="form" method="" action="{{route('verification',$mobile)}}">
                <br><br><br><br><br>
              <h5 style="font-family: 'Yekan','Roboto-Regular';" class="description text-center">کد دریافتی را وارد کنید</h5>
              <div class="card-body">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="material-icons">smartphone</i>
                    </span>
                  </div>
                  <input type="text" class="form-control" placeholder="Verification Code ..">
                </div>
              </div>
              <div class="footer text-center">
                <a href="{{URL::asset('http://author.pielem.ir')}}" class="btn btn-primary btn-link btn-wd btn-lg">تایید کد</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@stop
