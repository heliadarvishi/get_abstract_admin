
@extends('main_main')
@section('content')
    <div class="wrapper-page">
        <div class="m-t-40 card-box">
            <div class="text-center">
                <!-- <h4 class="text-uppercase font-bold m-b-0" style="font-family: iransans;">تغییر پسورد</h4> -->
            </div>
            <div class="panel-body">
                @if(Session::has('errmsg'))<div class="alert alert-danger text-center">{{Session::get('errmsg')}}</div>@endif
                @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
                @if ($errors->any())
                    <div class="alert alert-danger text-center">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div  style="font-family: iransans;">
                    <div class = "text-center">
                        <div class="alert alert-info text-center">
                        <h5 style="font-family: iransans;"> رمز عبور جدیدی را ایجاد نمایید </h5>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{route('site.resset_password',$info = ['mobile'=> $data['mobile'],'verification_code'=>$data['verification_code']])}}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                    <div class="form-group">
                                        <input type="password" class="form-control" required name="new_password" placeholder="پسورد جدید"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" required name="confirm_password" placeholder="تکرار پسورد جدید"/>
                                    </div>
                                    <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop