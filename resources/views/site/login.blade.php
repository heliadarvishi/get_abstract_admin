@extends('main_main')
@section('content')
    <div class="wrapper-page">
        <div class="m-t-40 card-box">
            <div class="text-center">
                <!-- <h4 class="text-uppercase font-bold m-b-0" style="font-family: iransans;">Login</h4> -->
            </div>
            <div class="panel-body">
                <form class="form-horizontal m-t-20" action="{{route('login')}}" method="POST">
                    @if(session()->has('errmsg'))<div class="alert alert-danger text-center">{{session()->get('errmsg')}}</div>@endif
                    @if(session()->has('message'))<div class="alert alert-success text-center">{{session()->get('message')}}</div>@endif
                    <!-- <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" /> -->
                    {{csrf_field()}}
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="mobile" type="text" required placeholder="نام کاربری">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" required placeholder="رمز عبور">
                        </div>
                    </div>

                    <div class="form-group text-center m-t-30">
                        <div class="col-xs-12">
                            <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit" >ورود</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12">
                            <a href="" class="text-muted" data-toggle="modal" data-target="#forgetpass"><i class="fa fa-lock m-r-5" ></i> فراموشی رمز عبور</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ...................modal..........................-->
    <div id="" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="font-family: iransans;"> بازیابی رمز عبور</h4>
                </div>
            <div class="modal-body">
                <!-- <div class="alert alert-info text-center">لینکی جهت بازنشانی رمز عبور به ایمیلتان ارسال شد</div> -->
                <form action="" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label"> آدرس پست الکترونیکی </label>
                            <input type="email" class="form-control" required name="email" placeholder="آدرس پست الکترونیکی خود را وارد نمایید"/>
                        </div>
                    </div>
                    </div>
                    <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                </form>
            </div>
        </div>
    </div><!-- /.modal -->

     <!-- ...................modal..........................-->
     <div id="forgetpass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="font-family: iransans;"> بازیابی رمز عبور</h4>
              
                    <div class="modal-body">
                        <div class="card-box">
                            <div class = "text-center">
                                <div class="row">
                                    <div class ="zmdi zmdi-local-post-office zmdi-hc-2x" style="color:#35b8e0;"></div>
                                    <div class ="zmdi zmdi-smartphone-iphone zmdi-hc-4x" style="color:#35b8e0;"></div>
                                </div><br>
                                <div class="form-group text-center">
                                    <label class="control-label" style="color:#35b8e0;">{{env('APP_NAME')}} کد تایید را برای شما ارسال می کند</label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <form action="{{route('site.send_code')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="control-label">کد تایید را دریافت کنید</label>
                                    <input type="text" class="form-control" required name="mobile" placeholder="شماره موبایل خود را وارد نمایید"/>
                                </div>
                                <button type="submit" class="btn btn-info waves-effect waves-light">ارسال</button>
                            </form>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
@stop