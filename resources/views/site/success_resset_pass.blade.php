
@extends('main_main')
@section('content')
    <div class="wrapper-page">
        <div class="m-t-40 card-box">
            <div class="text-center">
                <!-- <h4 class="text-uppercase font-bold m-b-0" style="font-family: iransans;">تغییر پسورد</h4> -->
            </div>
            <div class="panel-body">
                @if(Session::has('errmsg'))<div class="alert alert-danger text-center">{{Session::get('errmsg')}}</div>@endif
                @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
                @if ($errors->any())
                    <div class="alert alert-danger text-center">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop