
@extends('main_main')
@section('content')
    <div class="wrapper-page">
        <div class="m-t-40 card-box">
            <div class="text-center">
                <h4 class="text-uppercase font-bold m-b-0" style="font-family: iransans;">بازیابی رمز عبور</h4>
            </div>
            <div class="panel-body">
                @if(Session::has('errmsg'))<div class="alert alert-danger text-center">{{Session::get('errmsg')}}</div>@endif
                @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
                <div  style="font-family: iransans;">
                    <div class="card-box">
                        <div class = "text-center">
                            <div class="row">
                                <div class ="zmdi zmdi-local-post-office zmdi-hc-2x" style="color:#35b8e0;"></div>
                                <div class ="zmdi zmdi-smartphone-iphone zmdi-hc-4x" style="color:#35b8e0;"></div>
                            </div><hr>
                             <div class="alert alert-info text-center">کد تایید جهت بازنشانی رمز عبور، برای شما ارسال شد</div> 
                        </div>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <form action="{{route('site.code-verification',$mobile)}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="form-group">
                                <input type="text" class="form-control" required name="code" placeholder="کد تایید را وارد نمایید"/>
                            </div>
                            <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                        </form>
                    </div>   
                </div>
            </div>
        </div>
    </div>
@stop