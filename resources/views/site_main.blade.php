<!DOCTYPE html>
<html lang="fa">
    <head>
        @include('partials.site._head')
    </head>
    @yield('body')
    <!-- <body class="index-page sidebar-collapse" style="font-family: 'Yekan','Roboto-Regular';"> -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKDMSK6" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        @include('partials.site._navbar')
        @yield('content')
        @include('partials.site._footer')
        @include('partials.site._scripts')
    </body>
</html>