@extends('admin_main')
@section('content')
@section('title','کانال های نویسنده ها')

<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
    @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card-box" style="font-family: iransans;">
        @if(!isset($resp))
            <a href="" class = "btn btn-bordred waves-effect w-md waves-light m-b-5 " style=" background-color: #50b2ca; color:#fff" data-toggle="modal" data-target="#newTerm">  +  اضافه کردن کانال جدید </a>
        @endif
    
    <div class="col-md-4">
        <form action="{{route('authorChannel.search')}}" method="post" role="form">
            <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button type="submit" style="background-color: #50b2ca; color: #fff;" class="btn waves-effect waves-light"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="example-input1-group2" name="search" class="form-control" placeholder="جستجو بر اساس عنوان">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4"></div>
    
    <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;"></h4>
    @if(count($channels) == 0 && $state == 0)
        <div class="alert alert-danger text-center">کانالی وجود ندارد </div>
    @else
        <table class="table table-striped m-0">
            <thead>
                <tr>
                    <th>#</td>
                    <th>تصویر</th>
                    <th> عنوان</th>
                    <th> وضعیت</th>
                    <th> نوع</th>
                    <th> توسط</th>
                    <th class="col-md-4"> بیشتر </th>
                </tr>
            </thead>
            <tbody>
            @if(Session::has('msge'))<div class="alert alert-success text-center">{{Session::get('msge')}}</div>@endif
            <?php $i=0; ?>
                @foreach($channels as $channel)
                    <tr>
                    <td>{{++$i}} </td>
                    <td> <img src="https://cdn.pielem.ir/uploads/images/channel_logo/{{$channel->logo}}" width="130" height="130" class="rounded-circle" alt="Sample image"></td>
                    <td>{{$channel->title}}</td></td>
                    <td>@if($channel->status == 2) <span class="label label-warning">در انتظار تایید</span> @elseif($channel->status == 1)<span class="label label-success">پذیرفته شده</span> @elseif($channel->status == 3)<span class="label label-danger">رد شده</span>@endif</td>                    
                    <td>{{$channel['category']['title']}}</td>
                    <td>{{$channel['user']['first_name']}} {{$channel['user']['last_name']}}</td>
                    <td>
                    @if($channel['user']['type'] == 3)
                        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#pending{{$channel->id}}"> <i class="ti ti-help"></i> </a>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#accept{{$channel->id}}"> <i class="ti ti-check"></i> </a>
                        <a href="#" class="btn btn-pink" data-toggle="modal" data-target="#reject{{$channel->id}}"> <i class="fa fa-remove"></i> </a>
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$channel->id}}"> <i class="fa fa-remove"></i> </a>
                    @else
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$channel->id}}"> <i class="fa fa-remove"></i> </a>
                    @endif
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table> 
        {{$channels->render()}}
    @endif

    <!--start of createModal channel -->
    <div id="newTerm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
               
                    <h4 class="modal-title" style="font-family: iransans;">افزودن نویسنده </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('channel.store')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label"></label>نوع کانال</label>
                                    <select id="category" class="form-control" name="type">
                                    @foreach($categories as $category)
                                     <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام کانال</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام کانال" name="title" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="logo" class="control-label">لوگو</label>
                                <input type="file" class="dropify"  name="logo" accept=".jpg , .png" data-default-file= "" data-max-file-size="2M"/>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->

 @foreach($channels as $channel)
    <!-- start of modal pending Modal-->
        <div id="pending{{$channel->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید  کانال "{{$channel->title}}" به وضعیت در انتظار تایید برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('channel.pending',$channel)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-warning">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
    
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal accept Modal-->
            <div id="accept{{$channel->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید  کانال "{{$channel->title}}" به وضعیت پذیرفته شده برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('channel.accept',$channel)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal reject Modal-->
            <div id="reject{{$channel->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید کانال "{{$channel->title}}" به وضعیت رد شده برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('channel.reject',$channel)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-pink">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

    <!-- start of modal deleteModal-->
    <div id="delete{{$channel->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="font-family: iransans;">در صورت تایید شما، کلیه ی مشخصات کانال "{{$channel->title}}" <span style="color:pink">به همراه تمامی چکیده های داخل آن </span>حذف خواهد شد. <br><br>آیا مایل به حذف می باشید؟ </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('authorChannel.delete',$channel)}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger">بله</button>
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endforeach
</div>
<script src="{{URL::asset('site/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('site/js/popper.min.js')}}"></script>
<script src="{{URL::asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('site/js/jquery.validate.min.js')}}"></script>
<script src="{{URL::asset('site/js/plugins.js')}}"></script>
<script src="{{URL::asset('site/js/active.js')}}"></script>

@stop
