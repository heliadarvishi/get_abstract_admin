@extends('admin_main')
@section('content')
@section('title','افزودن نویسنده ویژه')

<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
    @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card-box" style="font-family: iransans;">
        @if(!isset($resp))
            <a href="" class = "btn btn-bordred waves-effect w-md waves-light m-b-5 " style=" background-color: #50b2ca; color:#fff" data-toggle="modal" data-target="#newTerm">  +  اضافه کردن نویسنده ویژه جدید </a>
        @endif
    
    <div class="col-md-4">
        <form action="{{route('superAuthor.search')}}" method="post" role="form">
        <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button type="submit" style="background-color: #50b2ca; color: #fff;" class="btn waves-effect waves-light"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="example-input1-group2" name="search" class="form-control" placeholder="جستجو">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4"></div>
   
    <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;"></h4>
    @if(count($super_authors) == 0 && $state == 0) 
        <div class="alert alert-danger text-center">شما تا کنون نویسنده ویژه ای اضافه نکرده اید </div>
    @else
        <table class="table table-striped m-0">
            <thead>
                <tr>
                    <th>#</td>
                    <th>تصویر</th>
                    <th> نام </th>
                    <th> نام خانوادگی</th>
                    <th> شماره موبایل</th>
                    <th> درخواست ها</th>
                    <th class="col-md-4"> بیشتر </th>
                </tr>
            </thead>
            <tbody>
            @if(Session::has('msge'))<div class="alert alert-success text-center">{{Session::get('msge')}}</div>@endif
            <?php $i=0; ?>
                @foreach($super_authors as $author)
                    <tr>
                    <td>{{++$i}} </td>
                    <td> <img src="https://cdn.pielem.ir/uploads/images/profile_pic/{{$author->avatar}}" width="130" height="130" class="rounded-circle" alt="Sample image"></td>
                    <td>{{$author->first_name}}</td>
                    <td>{{$author->last_name}}</td>
                    <td>{{$author->mobile}}</td>
                    <td>
                        @if($author->request == 2)
                            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#request{{$author->id}}">پاسخ به درخواست</a>
                        @else
                            <i class="ti-layout-line-solid"></i>
                        @endif
                    </td>
                    <td>
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#info{{$author->id}}"> <i class="ti ti-info-alt"></i> </a>
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$author->id}}"> <i class="fa fa-remove"></i> </a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table> 
        {{$super_authors->render()}}
    @endif

    <!--start of createModal new -->
    <div id="newTerm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
               
                    <h4 class="modal-title" style="font-family: iransans;">افزودن نویسنده ویژه </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.addSuperAuthor')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام" name="first_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام خانوادگی</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام خانوادگی" name="last_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label"> شماره موبایل</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="شماره موبایل" name="mobile" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">پسورد</label>
                                    <input type="password" class="form-control" id="field-1" placeholder="پسورد" name="password" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"> درباره نویسنده </label>
                                    <textarea id="elm1" name="about"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="pic" class="control-label">عکس پروفایل</label>
                                <input type="file" class="dropify"  name="avatar" accept=".jpg , .png" data-default-file= "" data-max-file-size="2M"/>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->

    @foreach($super_authors as $author)
        <!-- start of modal deleteModal-->
        <div id="delete{{$author->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none; font-family: iransans;">
            <div class="modal-dialog" style="font-family: iransans;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" style="font-family: iransans;">در صورت تایید شما، "کلیه ی مشخصات و آثار این نویسنده" حذف خواهد شد. <br><br>آیا مایل به حذف می باشید؟ </h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('superAuthor.delete',$author->id)}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger">بله</button>
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->

        <div id="info{{$author->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="font-family: iransans;">
                <div class="modal-content p-0 b-0">
                    <div class="panel panel-color panel-success">
                        <div class="panel-heading">
                            <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="panel-title">{{$author->first_name}} {{$author->last_name}}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <h5 style="font-family: iransans;" >  نام کاربری : <span class="label label-success">@if($author->user_name == null) ندارد @else {{$author->user_name}} @endif</span>  </h5>
                                    </div> 
                                    <div class="col-md-6">
                                        <h5 style="font-family: iransans;" > تعداد چکیده ها : <span class="label label-success">{{countSummaryByUserId($author->id)}}</span> </h5>
                                    </div>
                                </div>
                                <br><br>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"> درباره ی نویسنده </label>
                                        <br>
                                        <div class="card-box">
                                            {{$author->about}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

         <div id="request{{$author->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="font-family: iransans;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" style="font-family: iransans;"> <span style="color:blue"> "{{$author->first_name}} {{$author->last_name}}" درخواست تبدیل به نویسنده ی معمولی شدن دارد. با این درخواست موافقید؟ </span> <br><br></h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <form action="{{route('superAuthor.accept_request',$author->id)}}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success">بله، درخواست پذیرفته شود</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form action="{{route('superAuthor.reject_request',$author->id)}}" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-pink">خیر، درخواست رد شود</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
    @endforeach
</div>

@stop
