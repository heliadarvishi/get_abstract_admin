@extends('admin_main')
@section('content')
@section('title','داشبورد')
   
<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
     @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  <div class="row">           
    <div class = "col-lg-3 col-md-6">
        <div class="card-box">
            <div class = "text-center">
                <div class="row" >
                    <div class ="zmdi zmdi-edit zmdi-hc-2x" style="color:#ff3300;"></div>
                    <div class ="zmdi zmdi-edit zmdi-hc-4x" style="color:#ff3300;"></div>
                    <div class ="zmdi zmdi-edit zmdi-hc-2x" style="color:#ff3300;"></div>
                </div>
                <div class="widget-detail-1">
                    <h4 class="p-t-10 m-b-0">{{$authorCount}}</h2>
                    <p class="header-title">نویسنده های عادی</p>
                </div>
            </div>
        </div>
    </div><!-- end col -->

    <div class = "col-lg-3 col-md-6">
        <div class="card-box">
            <div class = "text-center">
                <div class="row" >
                    <div class =" zmdi zmdi-assignment-o zmdi-hc-2x" style="color:#3399ff;"></div>
                    <div class =" zmdi zmdi-assignment-o zmdi-hc-4x" style="color:#3399ff;"></div>
                    <div class =" zmdi zmdi-assignment-o zmdi-hc-2x" style="color:#3399ff;"></div>
                </div>
                <div class="widget-detail-1">
                    <h4 class="p-t-10 m-b-0"> {{$superAuthorCount}} </h2>
                    <p class="header-title"> نویسنده های ویژه </p>
                </div>
            </div>
        </div>
    </div><!-- end col -->

    <div class = "col-lg-3 col-md-6">
        <div class="card-box">
            <div class = "text-center">
                <div class="row" >
                    <div class ="zmdi zmdi-volume-mute zmdi-hc-2x" style="color: #ff9900;"></div>
                    <div class ="zmdi zmdi-volume-mute zmdi-hc-4x" style="color: #ff9900;"></div>
                    <div class ="zmdi zmdi-volume-mute zmdi-hc-2x" style="color: #ff9900;"></div>
                </div>
                <div class="widget-detail-1">
                    <h4 class="p-t-10 m-b-0"> {{$channelCount}} </h2>
                    <p class="header-title"> کانال ها </p>
                </div>
            </div>
        </div>
    </div><!-- end col -->

    <div class = "col-lg-3 col-md-6">
        <div class="card-box">
            <div class = "text-center">
                <div class="row" >
                    <div class ="zmdi zmdi-collection-text zmdi-hc-2x" style="color: #50b2ca;"></div>
                    <div class ="zmdi zmdi-collection-text zmdi-hc-4x" style="color: #50b2ca;"></div>
                    <div class ="zmdi zmdi-collection-text zmdi-hc-2x" style="color: #50b2ca;"></div>
                </div>
                <div class="widget-detail-1">
                    <h4 class="p-t-10 m-b-0"> {{$summaryCount}}</h2>
                    <p class="header-title"> چکیده ها </p>
                </div>
            </div>
        </div>
    </div><!-- end col -->
  </div> <!-- end row -->

    <!-- Start content -->                  
    <div class="row">
        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;">تغییر پسورد</h4>
                <form  action ="{{route('dashboard.editPass')}}" method="post">
                <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                    <div id="basicwizard" class=" pull-in">                                            
                        <div class="tab-content b-0 m-b-0">
                            <div class="row">
                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="userName"> پسورد فعلی </label>
                                    <div class="col-md-9">
                                        <input class="form-control required" id="userName" name="old_password" type="password">
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="password"> پسورد جدید </label>
                                    <div class="col-md-9">
                                        <input id="password" name="password" type="password" class="required form-control">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="confirm">تکرار پسورد </label>
                                    <div class="col-md-9">
                                        <input id="confirm" name="password_confirmation" type="password" class="required form-control">
                                    </div>
                                </div>
                            </div>
                                <button  class="btn btn-success btn-bordred waves-effect waves-light m-b-5">ثبت</button>                           
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end col -->

        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;">ویرایش اطلاعات</h4>
                <form action ="{{route('dashboard.editInfo')}}" method="post">
                <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                    <div id="basicwizard" class=" pull-in">                                            
                        <div class="tab-content b-0 m-b-0">
                            <div class="row">
                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="firstName"> نام کاربری</label>
                                    <div class="col-md-9">
                                        <input class="form-control required" value="{{$user->user_name}}" id="user_name" name="user_name" type="text">
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="first_name">نام  </label>
                                    <div class="col-md-9">
                                        <input id="first_name" name="first_name" value="{{$user->first_name}}" type="text" class="required form-control">
                                    </div>
                                </div>

                                 <div class="form-group clearfix">
                                    <label class="col-md-3 control-label " for="last_name">  نام خانوادگی </label>
                                    <div class="col-md-9">
                                        <input id="last_name" name="last_name" value="{{$user->last_name}}" type="text" class="required form-control">
                                    </div>
                                </div>
                               
                            </div>
                        
                            <button  class="btn btn-success btn-bordred waves-effect waves-light m-b-5">ثبت</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- end col -->

    </div><!-- End row -->
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
</div>
@stop