@extends('admin_main')
@section('content')
@section('title','پرداخت ها')
   
<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
     @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

</div>
@stop