@extends('admin_main')
@section('content')
@section('title','کانال های در انتظار تایید')

<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
    @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card-box" style="font-family: iransans;">
        @if(!isset($resp))
            <a href="" class = "btn btn-bordred waves-effect w-md waves-light m-b-5 " style=" background-color: #50b2ca; color:#fff" data-toggle="modal" data-target="#newTerm">  +  اضافه کردن کانال جدید </a>
        @endif
    
    <div class="col-md-4">
        <form action="{{route('pendingChannel.search')}}" method="post" role="form">
            <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button type="submit" style="background-color: #50b2ca; color: #fff;" class="btn waves-effect waves-light"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="example-input1-group2" name="search" class="form-control" placeholder="جستجو بر اساس عنوان">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4"></div>

    <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;"></h4>
    @if(count($channels) == 0 && $state == 0)
        <div class="alert alert-danger text-center">کانالی موجود نیست </div>
    @else
        <table class="table table-striped m-0">
            <thead>
                <tr>
                    <th>#</td>
                    <th>تصویر</th>
                    <th> عنوان</th>
                    <th> نوع</th>
                    <th class="col-md-4"> بیشتر </th>
                </tr>
            </thead>
            <tbody>
            @if(Session::has('msge'))<div class="alert alert-success text-center">{{Session::get('msge')}}</div>@endif
            <?php $i=0; ?>
                @foreach($channels as $channel)
                    <tr>
                    <td>{{++$i}} </td>
                    <td> <img src="https://cdn.pielem.ir/uploads/images/channel_logo/{{$channel->logo}}" width="130" height="130" class="rounded-circle" alt="Sample image"></td>
                    <td>{{$channel->title}}</td></td>
                    <td>{{$channel['category']['title']}}</td>
                    <td>
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#info{{$channel->id}}"> <i class="ti ti-info-alt"></i> </a>
                        <!-- <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#edit"> <i class="fa fa-wrench"></i> </a>
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add"> <i class="ti ti-write"></i> </a> -->
                        <!-- <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$channel->id}}"> <i class="fa fa-remove"></i> </a> -->
                    </td>
                    </tr>
                @endforeach
            </tbody>
        </table> 
        {{$channels->render()}}
    @endif

    <!--start of createModal new food -->
    <div id="newTerm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
               
                    <h4 class="modal-title" style="font-family: iransans;">افزودن نویسنده </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('channel.store')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label"></label>نوع کانال</label>
                                    <select id="category" class="form-control" name="type">
                                    @foreach($categories as $category)
                                     <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام کانال</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام کانال" name="title" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="logo" class="control-label">لوگو</label>
                                <input type="file" class="dropify"  name="logo" accept=".jpg , .png" data-default-file= "" data-max-file-size="2M"/>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->

    @foreach($channels as $channel)
        <div id="info{{$channel->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="font-family: iransans;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                        <h4 class="modal-title" style="font-family: iransans;"> <span class="label label-success">{{$channel->title}}</span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h5 style="font-family: iransans;" > سازنده کانال : <span class="label label-success">{{getNameChannelMaker($channel->user_id)['first_name']}} {{getNameChannelMaker($channel->user_id)['last_name']}}</span>  </h5>
                                </div> 
                                <div class="col-md-6">
                                    <h5 style="font-family: iransans;" > تعداد چکیده ها : <span class="label label-success">{{countSummaryByChannelId($channel->id)}}</span> </h5>
                                </div>
                            </div>
                            <br><br><br>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label"> لیست چکیده ها </label>
                                    <br>
                                    <div class="card-box">
                                        @foreach(listSummaryByChannelId($channel->id) as $summary)
                                            {{$summary['title']}}
                                            -
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->

        <!-- start of modal deleteModal-->
        <div id="" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="font-family: iransans;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" style="font-family: iransans;">در صورت تایید شما، کلیه ی مشخصات  حذف خواهد شد. <br><br>آیا مایل به حذف می باشید؟ </h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                            <div class="text-center">
                                <button type="submit" class="btn btn-danger">بله</button>
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
    @endforeach


</div>
<script src="{{URL::asset('site/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('site/js/popper.min.js')}}"></script>
<script src="{{URL::asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('site/js/jquery.validate.min.js')}}"></script>
<script src="{{URL::asset('site/js/plugins.js')}}"></script>
<script src="{{URL::asset('site/js/active.js')}}"></script>
<script>
        $('#countries').on('change',function(e){
        var p_id = e.target.value;
        if(p_id == 103)
        {
            //ajax
            $.get('get_partial_states_iran/'+p_id,function(data){
            //success data
            $('#states').empty();
            $.each(data, function (index,cityObj){
            $('#states').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
            });
            });  
        }
        else
        {
            //ajax
            $.get('get_partial_states/'+p_id,function(data){
            //success data
            $('#states').empty();
            $.each(data, function (index,cityObj){
            $('#states').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
            });
            });  
        }
      
            });
</script>
<script>
    $('#states').on('change',function(e){
    var p_id = e.target.value;
    
    // alert(document.getElementById("countries").value);
    if(document.getElementById("countries").value == 103)
    {
        //ajax
        $.get('get_partial_cities_iran/'+p_id,function(data){
        //success data
        $('#cities').empty();
        $.each(data, function (index,cityObj){
        $('#cities').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
        });
        });  
    }
    else
    {
        //ajax
        $.get('get_partial_cities/'+p_id,function(data){
        //success data
        $('#cities').empty();
        $.each(data, function (index,cityObj){
        $('#cities').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
        });
        });  
    }
    
    });
</script>
@stop
