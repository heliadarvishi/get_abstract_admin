@extends('admin_main')
@section('content')
@section('title',' چکیده ها ')
  <!-- $summary['category']['title'] -->
<div class="container" style="font-family: iransans;">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
    @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    @if ($errors->any())
        <div class="alert alert-danger text-center">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card-box" style="font-family: iransans;">
    
    <div class="col-md-4">
        <form action="{{route('summary.search')}}" method="post" role="form">
            <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button type="submit" style="background-color: #50b2ca; color: #fff;" class="btn waves-effect waves-light"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="example-input1-group2" name="search" class="form-control" placeholder="جستجو بر اساس عنوان">
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-4"></div>
    
    <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;"></h4><br>
    @if(count($summaries) == 0 && $state == 0)
        <div class="alert alert-danger text-center">چکیده ای موجود نیست </div>
    @else
        <table class="table table-striped m-0">
            <thead>
                <tr>
                    <th>#</td>
                    <th> تصویر روی جلد</th>
                    <th> عنوان</th>
                    <th> توسط</th>
                    <th> مربوط به کانال</th>
                    <th>وضعیت</th>
                    <th> دانلود فایل چکیده</th>
                    <th class="col-md-4"> بیشتر </th>
                </tr>
            </thead>
            <tbody>
            @if(Session::has('msge'))<div class="alert alert-success text-center">{{Session::get('msge')}}</div>@endif
            <?php $i=0; ?>
                @foreach($summaries as $summary)
                    <tr>
                        <td>{{++$i}} </td>
                        <td> <img src="https://cdn.pielem.ir/uploads/images/summary_cover/{{$summary->cover}}" width="130" height="130" class="rounded-circle" alt="Sample image"></td>
                        <td>{{$summary->title}}</td>
                        <td>{{$summary['user']['first_name']}} {{$summary['user']['last_name']}}</td>
                        <td>{{$summary['channel']['title']}}</td>
                        <th>@if($summary['status'] == 1) <span class="label label-success">پدیرفته شده</span> @elseif($summary['status'] == 2) <span class="label label-warning">در انتظار تایید</span>@elseif($summary['status'] == 3) <span class="label label-danger">رد شده</span> @endif</th>
                        <th><a href="" class="" data-toggle="modal" data-target="#download{{$summary->id}}"> دانلود فایل</a></th>
                        <td>
                            @if($summary['user']['type'] == 3)
                                <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#pending{{$summary->id}}"> <i class="ti ti-help"></i> </a>
                                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#accept{{$summary->id}}"> <i class="ti ti-check"></i> </a>
                                <a href="#" class="btn btn-pink" data-toggle="modal" data-target="#reject{{$summary->id}}"> <i class="fa fa-remove"></i> </a>
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$summary->id}}"> <i class="zmdi zmdi-delete"></i></a>
                            @else
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$summary->id}}"> <i class="zmdi zmdi-delete"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table> 
        {{$summaries->render()}}
    @endif

    @foreach($summaries as $summary)
        <!-- start of modal download Modal-->
            <div id="download{{$summary->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>فایل های مربوط به چکیده ی {{$summary->title}} </h4>
                        </div>
                        <div class="modal-body">
                            @foreach($summary['file'] as $file)
                                <a href="https://cdn.pielem.ir/uploads/images/summary_file/{{$file['url']}}" class="btn btn-success btn-trans " style="background-color:#78c5d2" download> @if($file['type'] == 1) <p class ="fa fa-file-pdf-o fa-hc-2x" style="color:#000;"></p> @elseif($file['type'] == 2) <p class ="fa fa-file-word-o fa-hc-2x" style="color:#000;"></p>  @elseif($file['type'] == 3) <p class ="fa fa-file-sound-o fa-hc-2x" style="color:#000;"></p>  @elseif($file['type'] == 4)  <p class ="fa fa-file-movie-o fa-hc-2x" style="color:#000;"></p>  @endif </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal pending Modal-->
            <div id="pending{{$summary->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید چکیده ی "{{$summary->title}}" به وضعیت در انتظار تایید برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('summary.pending',$summary)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-warning">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal accept Modal-->
            <div id="accept{{$summary->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید چکیده ی "{{$summary->title}}" به وضعیت پذیرفته شده برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('summary.accept',$summary)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal reject Modal-->
            <div id="reject{{$summary->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید چکیده ی "{{$summary->title}}" به وضعیت رد شده برود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('summary.reject',$summary)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-pink">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

        <!-- start of modal delete Modal-->
            <div id="delete{{$summary->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog" style="font-family: iransans;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" style="font-family: iransans;"><br><br>آیا میخواهید چکیده ی "{{$summary->title}}" حذف شود؟</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('summary.delete',$summary)}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-danger">بله</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /.modal -->

    @endforeach
   
</div>
<script src="{{URL::asset('site/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('site/js/popper.min.js')}}"></script>
<script src="{{URL::asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('site/js/jquery.validate.min.js')}}"></script>
<script src="{{URL::asset('site/js/plugins.js')}}"></script>
<script src="{{URL::asset('site/js/active.js')}}"></script>

@stop
