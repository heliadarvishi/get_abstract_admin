@extends('developer_main')
@section('content')
@section('title','افزودن ادمین')

<div class="container">
    @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
    @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
    <div class="card-box" style="font-family: iransans;">
    @if(!isset($resp))
        <a href="" class = "btn btn-bordred waves-effect w-md waves-light m-b-5 " style=" background-color: #50b2ca; color:#fff" data-toggle="modal" data-target="#newTerm">  اضافه کردن ادمین جدید +</a>
    @endif
    
    <div class="col-md-4">
        <form action="" method="post" role="form">
        <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-btn">
                    <button type="submit" style="background-color: #50b2ca; color: #fff;" class="btn waves-effect waves-light"><i class="fa fa-search"></i></button>
                    </span>
                    <input type="text" id="example-input1-group2" name="search" class="form-control" placeholder="Search">
                </div>
            </div>
        </form>
    </div>
   
    <h4 class="header-title m-t-0 m-b-30" style="font-family: iransans;"></h4>
    @if(count($admins) == 0)
        <div class="alert alert-danger text-center">شما تا کنون ادمینی اضافه نکرده اید </div>
    @else
        <table class="table table-striped m-0">
            <thead>
                <tr>
                    <th>#</td>
                    <th> نام </th>
                    <th> نام خانوادگی </th>
                    <th> شماره موبایل</th>
                    <th class="col-md-4"> بیشتر </th>
                </tr>
            </thead>
            <tbody>
            <?php $i=0; ?>
            @foreach($admins as $admin)
                <tr>
                <td>{{++$i}} </td>
                <td>{{$admin->first_name}}</td>
                <td>{{$admin->last_name}}</td>
                <td>{{$admin->mobile}}</td>
                <td>
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#info"> <i class="ti ti-info-alt"></i> </a>
                    <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#edit"> <i class="fa fa-wrench"></i> </a>
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add"> <i class="ti ti-write"></i> </a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$admin->id}}"> <i class="fa fa-remove"></i> </a>
                </td>
                </tr>
            @endforeach
            </tbody>
        </table> 
        {{$admins->render()}}
    @endif

    <!--start of createModal new food -->
    <div id="newTerm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
               
                    <h4 class="modal-title" style="font-family: iransans;">افزودن ادمین </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام" name="first_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">نام خانوادگی</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="نام خانوادگی" name="last_name" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label"> شماره موبایل</label>
                                    <input type="text" class="form-control" id="field-1" placeholder="شماره موبایل" name="mobile" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">پسورد</label>
                                    <input type="password" class="form-control" id="field-1" placeholder="پسورد" name="password" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
@foreach($admins as $admin)
    <!-- start of modal deleteModal-->
    <div id="delete{{$admin->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="font-family: iransans;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="font-family: iransans;">در صورت تایید شما، کلیه ی مشخصات {{$admin->full_name}} حذف خواهد شد. <br><br>آیا مایل به حذف می باشید؟ </h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.delete',$admin->id)}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" id="csrf-token" required value="{{ Session::token() }}" />
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger">بله</button>
                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">انصراف</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endforeach
</div>
<script src="{{URL::asset('site/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::asset('site/js/popper.min.js')}}"></script>
<script src="{{URL::asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('site/js/jquery.validate.min.js')}}"></script>
<script src="{{URL::asset('site/js/plugins.js')}}"></script>
<script src="{{URL::asset('site/js/active.js')}}"></script>
<script>
        $('#countries').on('change',function(e){
        var p_id = e.target.value;
        if(p_id == 103)
        {
            //ajax
            $.get('get_partial_states_iran/'+p_id,function(data){
            //success data
            $('#states').empty();
            $.each(data, function (index,cityObj){
            $('#states').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
            });
            });  
        }
        else
        {
            //ajax
            $.get('get_partial_states/'+p_id,function(data){
            //success data
            $('#states').empty();
            $.each(data, function (index,cityObj){
            $('#states').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
            });
            });  
        }
      
            });
</script>
<script>
    $('#states').on('change',function(e){
    var p_id = e.target.value;
    
    // alert(document.getElementById("countries").value);
    if(document.getElementById("countries").value == 103)
    {
        //ajax
        $.get('get_partial_cities_iran/'+p_id,function(data){
        //success data
        $('#cities').empty();
        $.each(data, function (index,cityObj){
        $('#cities').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
        });
        });  
    }
    else
    {
        //ajax
        $.get('get_partial_cities/'+p_id,function(data){
        //success data
        $('#cities').empty();
        $.each(data, function (index,cityObj){
        $('#cities').append('<option value="'+cityObj.id+'">'+cityObj.name+'</option>');
        });
        });  
    }
    
    });
</script>
@stop
