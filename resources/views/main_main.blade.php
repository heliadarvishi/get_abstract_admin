
<html lang="en" dir="rtl">
<head>
    @include('partials.admin._head')
</head>
<body style="font-family: iransans;">
    @yield ('content')  
    @include('partials.admin._scripts')
</body>
</html>