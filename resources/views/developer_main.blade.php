<!DOCTYPE html>
<html lang="en" dir="rtl">
    <head>
          @include('partials.developer._head')
    </head>
    <body class="fixed-left">
        <div id="wrapper">
             @include('partials.developer._sidebar')
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        @yield('content')
                    </div> <!-- container -->
                </div> <!-- content -->
                   @include('partials.developer._footer')
            </div>
        </div>
        <!-- END wrapper -->
        @include('partials.developer._scripts')
    </body>
</html>