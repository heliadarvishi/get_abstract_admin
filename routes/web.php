<?php

Route::get('/login',["as" => "site.login" , "uses" => "AutController@show"]);
Route::post('/login',["as" => "login" , "uses" => "AutController@login"]);

// Route::post('/verification/{mobile}',["as" => "verification" , "uses" => "RegisterController@verification"]);

//reset password opration
Route::post('/send-code',["as" => "site.send_code" , "uses" => "AutController@sendCode"]);
Route::get('/send-sms-code',["as" => "site.send_sms_code" , "uses" => "AutController@sendSmsCode"]);

Route::post('/code-verification/{mobile}',["as" => "site.code-verification" , "uses" => "AutController@codeVerification"]);

Route::post('/resset-password/{mobile}/{verification_code}',["as" => "site.resset_password" , "uses" => "AutController@ressetPassword"]);
Route::get('/resset-pass/{mobile}/{verification_code}',["as" => "site.resset_pass" , "uses" => "AutController@ressetPass"]);

// logout
Route::get('/logout' , ['as' => 'site.logout' , 'uses' => 'AutController@logout']);

Route::middleware(['is_developer'])->group(function () {
    Route::namespace('Developer')->prefix('developer')->group(function () {
        //dashboard
        Route::get('/dashboard/show', ["as" => "developer.dashboard", "uses" => "DashboardController@show"]);
        Route::post('/dashboard/edit-password', ["as" => "dashboard.editPass", "uses" => "DashboardController@editPass"]);
        Route::post('/dashboard/edit-info', ["as" => "dashboard.editInfo", "uses" => "DashboardController@editInfo"]);

        //admin
        Route::get('/admin/show', ["as" => "developer.admin", "uses" => "AdminController@show"]);
        Route::post('/admin/store', ["as" => "admin.store", "uses" => "AdminController@store"]);
        Route::post('/admin/delete/{id}', ["as" => "admin.delete", "uses" => "AdminController@destroy"]);
    });
});

Route::middleware(['is_admin'])->group(function () {
    Route::namespace('Admin')->prefix('admin')->group(function () {
        // dashboard
        Route::get('/dashboard/show', ["as" => "admin.dashboard", "uses" => "DashboardController@show"]);
        Route::post('/dashboard/edit-password', ["as" => "dashboard.editPass", "uses" => "DashboardController@editPass"]);
        Route::post('/dashboard/edit-info', ["as" => "dashboard.editInfo", "uses" => "DashboardController@editInfo"]);
        Route::post('/dashboard/profile', ["as" => "dashboard.profile", "uses" => "DashboardController@profileStore"]);

        // author
        Route::get('/author/show', ["as" => "admin.author", "uses" => "AuthorController@show"]);
        Route::post('/author/store', ["as" => "admin.addAuthor", "uses" => "AuthorController@store"]);
        Route::post('/author/delete/{author}', ["as" => "author.delete", "uses" => "AuthorController@destroy"]);
        Route::post('/author/search', ["as" => "author.search", "uses" => "AuthorController@search"]);
        Route::post('/author/accept-request/{id}', ["as" => "author.accept_request", "uses" => "AuthorController@acceptRequest"]);
        Route::post('/author/reject-request/{id}', ["as" => "author.reject_request", "uses" => "AuthorController@rejectRequest"]);

        // super author
        Route::get('/super-author/show', ["as" => "admin.superAuthor", "uses" => "SuperAuthorController@show"]);
        Route::post('/super-author/store', ["as" => "admin.addSuperAuthor", "uses" => "SuperAuthorController@store"]);
        Route::post('/super-author/delete/{author}', ["as" => "superAuthor.delete", "uses" => "SuperAuthorController@destroy"]);
        Route::post('/super-author/search', ["as" => "superAuthor.search", "uses" => "SuperAuthorController@search"]);
        Route::post('/super-author/accept-request/{id}', ["as" => "superAuthor.accept_request", "uses" => "SuperAuthorController@acceptRequest"]);
        Route::post('/super-author/reject-request/{id}', ["as" => "superAuthor.reject_request", "uses" => "SuperAuthorController@rejectRequest"]);

        // all public channels
        Route::get('/channel/show', ["as" => "admin.channel", "uses" => "ChannelController@show"]);
        Route::post('/channel/store', ["as" => "channel.store", "uses" => "ChannelController@store"]);
        Route::post('/channel/delete/{channel}', ["as" => "channel.delete", "uses" => "ChannelController@destroy"]);
        Route::post('/channel/search', ["as" => "channel.search", "uses" => "ChannelController@search"]);

        // author channels
        Route::get('/author-channel/show', ["as" => "admin.authorChannel", "uses" => "AuthorChannelController@show"]);
        Route::post('/author-channel/delete/{channel}', ["as" => "authorChannel.delete", "uses" => "AuthorChannelController@destroy"]);
        Route::post('/channel/change-status-pending/{channel}', ["as" => "channel.pending", "uses" => "AuthorChannelController@changeStatePending"]);
        Route::post('/channel/change-status-accept/{channel}', ["as" => "channel.accept", "uses" => "AuthorChannelController@changeStateAccept"]);
        Route::post('/channel/change-status-reject/{channel}', ["as" => "channel.reject", "uses" => "AuthorChannelController@changeStateReject"]);
        Route::post('/author-channel/search', ["as" => "authorChannel.search", "uses" => "AuthorChannelController@search"]);

        // accept channel
        Route::get('/accept-channel/show', ["as" => "admin.acceptChannel", "uses" => "AcceptChannelController@show"]);
        Route::post('/accept-channel/search', ["as" => "acceptChannel.search", "uses" => "AcceptChannelController@search"]);
       
        // pending channel
        Route::get('/pending-channel/show', ["as" => "admin.pendingChannel", "uses" => "PendingChannelController@show"]);
        Route::post('/pending-channel/search', ["as" => "pendingChannel.search", "uses" => "PendingChannelController@search"]);

        // reject channel
        Route::get('/reject-channel/show', ["as" => "admin.rejectChannel", "uses" => "RejectChannelController@show"]);
        Route::post('/reject-channel/search', ["as" => "rejectChannel.search", "uses" => "RejectChannelController@search"]);

        // summary
        Route::get('/summary/show', ["as" => "admin.summary", "uses" => "SummaryController@show"]);
        Route::post('/summary/change-status-pending/{summary}', ["as" => "summary.pending", "uses" => "SummaryController@changeStatePending"]);
        Route::post('/summary/change-status-accept/{summary}', ["as" => "summary.accept", "uses" => "SummaryController@changeStateAccept"]);
        Route::post('/summary/change-status-reject/{summary}', ["as" => "summary.reject", "uses" => "SummaryController@changeStateReject"]);
        Route::post('/summary-delete/{summary}', ["as" => "summary.delete", "uses" => "SummaryController@destroy"]);
        Route::post('/summary/search', ["as" => "summary.search", "uses" => "SummaryController@search"]);

        //payment
        Route::get('/payment/show', ["as" => "admin.payment", "uses" => "PaymentController@show"]);
    });  
});